#!/bin/bash

SESSION_TYPE="local"

if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
  SESSION_TYPE=remote/ssh
else
  case $(ps -o comm= -p "$PPID") in
    sshd|*/sshd) SESSION_TYPE=remote/ssh;;
  esac
fi

if [ $SESSION_TYPE == "local" ]; then
    ac=`ps aux | grep -i socat | grep -wv grep | awk '{ print $2 }'`
    if [ -z $ac ]; then
    sc=`which socat`
    if [ -z $sc ]; then
        echo "This application needs socat installed"
        exit 100
    fi
    echo "Launching socat"
    socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\" &
    fi

    xq=`ps aux | grep -i xquartz | grep -wv "grep\|socat" | awk '{ print $2 }' | wc -l`
    if [ $xq = 0 ]; then
        echo "Launching XQuartz"
        open -a XQuartz
    fi

    echo "Launching $1"

    docker run \
    -it \
    --net=host \
    -e DISPLAY=docker.for.mac.host.internal:0 \
    -v /tmp/.X11-unix \
    -v "$HOME/.ssh:/home/user/.ssh" \
    -v "$HOME/.Xauthority:/home/user/.Xauthority:rw" \
    -v"$(pwd)/user:/home/user" \
    $(basename `pwd`) \
    "$1"
else
    echo "Launching $1"

    docker run \
    -it \
    --net=host \
    --env="DISPLAY" \
    -v /tmp/.X11-unix \
    -v "$HOME/.ssh:/home/user/.ssh" \
    -v "$HOME/.Xauthority:/home/user/.Xauthority:rw" \
    -v"$(pwd)/user:/home/user" \
    $(basename `pwd`) \
    "$1"
fi

