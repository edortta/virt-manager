FROM ubuntu:22.04
LABEL maintainer="edortta71@gmail.com"

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y
RUN apt-get install axel curl wget -y

RUN apt-get install x11-apps -y

RUN apt-get install nano -y

RUN apt-get install virt-manager -y

RUN apt-get install openssh-client -y

RUN apt-get install -y dbus-x11

RUN useradd -ms /bin/bash -g root -G sudo -u 1000 user
RUN echo "user:user" | chpasswd
# RUN ssh-keygen -A

USER user
WORKDIR /home/user
ENV DISPLAY :0
ENV USER_HOME_DIR /home/user

EXPOSE 22


